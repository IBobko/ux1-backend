package com.ingrammicro.ux1.backend;


import org.quartz.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
public class Ux1BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ux1BackendApplication.class, args);
	}

	@Bean
	public JobDetail cianJobDetail() {
		return JobBuilder.newJob(LogJob.class).withIdentity("job")
				.storeDurably()
				.build();
	}

	@Bean
	public Trigger sampleJobTrigger() {
		SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder
				.simpleSchedule()
				.withIntervalInSeconds(1)
				.repeatForever();
		return TriggerBuilder.newTrigger().forJob(cianJobDetail())
				.withIdentity("trigger")
				.withSchedule(scheduleBuilder)
				.build();
	}

}

